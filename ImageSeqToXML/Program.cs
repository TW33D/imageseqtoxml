﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageSeqToXML
{
    class MapObject
    {
        public int bottomMargin;
        public int topMargin;
        public string xml;
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<string> files = Directory.GetFiles(ConfigurationManager.AppSettings["InputPath"], "*." + ConfigurationManager.AppSettings["InputFiles"]).ToList();
            List<MapObject> map = File.ReadAllLines(ConfigurationManager.AppSettings["MapFile"]).ToList().Select(item => new MapObject()
            {
                bottomMargin = Int32.Parse(item.Split('|')[0].Split(':')[0]),
                topMargin = Int32.Parse(item.Split('|')[0].Split(':')[1]),
                xml = item.Split('|')[1],
            }).ToList();

            Parallel.ForEach(files, file =>
            {
            });
        }
    }
}
